package com.btsinfo.myappi;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

class PersoAdapter extends ArrayAdapter<Personnage> {
    Context context;

    public PersoAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public View getView(int position, View convertView, ViewGroup parent){
        View view;
        Personnage unPersonnage;
        view=null;

        if (convertView==null){
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.personnage_activity, parent, false);
        }else{
            view = convertView;
        }
        unPersonnage = getItem(position);
        TextView tvpTitre1 = (TextView)view.findViewById(R.id.tvPersonnage);
        TextView tvpTitre2 = (TextView)view.findViewById(R.id.tvDesc);
        ImageView imageView = (ImageView)view.findViewById(R.id.imgPersonnage);

        tvpTitre1.setText(unPersonnage.getNomP());
        tvpTitre2.setText(unPersonnage.getDesP());

        AssetManager manager = context.getAssets();

        InputStream open = null;

        try {
            open = manager.open(unPersonnage.getImgP());
            Bitmap bitmap = BitmapFactory.decodeStream(open);
            imageView.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return view;
    }

}
