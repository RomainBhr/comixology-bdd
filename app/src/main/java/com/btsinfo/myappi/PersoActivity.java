package com.btsinfo.myappi;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class PersoActivity extends AppCompatActivity {

    TextView tvPersonnage;
    TextView tvDesP;
    TextView tvDesc;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.personnage_activity);

        tvPersonnage = (TextView)findViewById(R.id.tvPersonnage);
        tvDesP = (TextView)findViewById(R.id.tvDesP);
        tvDesc = (TextView)findViewById(R.id.tvDesc);
        imageView = (ImageView) findViewById(R.id.imgPersonnage);

        Intent intent = getIntent();
        String unPersonnage = intent.getStringExtra("nomP");
        String uneDesc = intent.getStringExtra("DesUn");
        String uneDesP = intent.getStringExtra("desP");

        tvPersonnage.setText(unPersonnage);
        tvDesP.setText(uneDesP);
        tvDesc.setText(uneDesc);


        AssetManager manager = this.getAssets();
        InputStream open = null;
        try {
            open = manager.open(intent.getStringExtra("imgP"));
            Bitmap bitmap = BitmapFactory.decodeStream(open);
            imageView.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
