package com.btsinfo.myappi;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class MembreActivity extends AppCompatActivity {

    private DataBaseManager dbm;
    ListView lvListe;
    ArrayList<Personnage> ListePersonnage;
    TextView tvEquipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.membreactivity);

        lvListe = (ListView)findViewById(R.id.listView2);
        tvEquipe = (TextView)findViewById(R.id.tvEquipe);

        Intent intent = getIntent();
        int EquipeId = intent.getIntExtra("idE", 0);
        String EquipeNom = intent.getStringExtra("nomE");
        tvEquipe.setText(EquipeNom);



        if (intent.getIntExtra("idE", 0) == 1){
            ListePersonnage = new DataBaseManager(this).lectureMembre(EquipeId);
        }

        if (intent.getIntExtra("idE", 0) == 2){
            ListePersonnage = new DataBaseManager(this).lectureMembre2(EquipeId);
        }

        if (intent.getIntExtra("idE", 0) == 3){
            ListePersonnage = new DataBaseManager(this).lectureMembre3(EquipeId);
        }

        lvListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                startViewActivity(i);
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        ListeAdapterP listeAdapterP = new ListeAdapterP(this, ListePersonnage);
        lvListe.setAdapter(listeAdapterP);
    }

    private void startViewActivity(int i) {
        Personnage unPersonnage = ListePersonnage.get(i);

        Intent intent = new Intent(this, PersoActivity.class);

        intent.putExtra("nomP",unPersonnage.getNomP());
        intent.putExtra("desP",unPersonnage.getDesP());
        intent.putExtra("DesUn",unPersonnage.getDesUn());
        intent.putExtra("imgP",unPersonnage.getImgP());

        startActivity(intent);

    }
}
