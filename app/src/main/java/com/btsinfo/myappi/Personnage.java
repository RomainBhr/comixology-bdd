package com.btsinfo.myappi;

public class Personnage {
    int IdE;
    int idP;
    String NomP;
    String DesP;
    String DesUn;
    String ImgP;


    public Personnage(int idE, int idP, String nomP, String desP, String desUn, String imgP) {
        this.IdE = idE;
        this.idP = idP;
        this.NomP = nomP;
        this.DesP = desP;
        this.DesUn = desUn;
        this.ImgP = imgP;
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public String getNomP() {
        return NomP;
    }

    public void setNomP(String nomP) {
        NomP = nomP;
    }

    public String getDesUn() {
        return DesUn;
    }

    public void setDesUn(String desUn) {
        DesUn = desUn;
    }

    public String getDesP() {
        return DesP;
    }

    public void setDesP(String desP) {
        DesP = desP;
    }

    public String getImgP() {
        return ImgP;
    }

    public void setImgP(String imgP) {
        ImgP = imgP;
    }

    public int getCatE() {
        return IdE;
    }

    public void setCatE(int idE) {
        IdE = idE;
    }
}
