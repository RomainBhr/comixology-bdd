package com.btsinfo.myappi;

public class Equipe {
    int idE;
    String NomE;
    String DesE;
    String ImgE;

    public Equipe(int idE, String nomE, String desE, String imgE) {
        this.idE = idE;
        NomE = nomE;
        DesE = desE;
        ImgE = imgE;
    }

    public int getIdE() {
        return idE;
    }

    public void setIdE(int idE) {
        this.idE = idE;
    }

    public String getNomE() {
        return NomE;
    }

    public void setNomE(String nomE) {
        NomE = nomE;
    }

    public String getDesC() {
        return DesE;
    }

    public void setDesC(String desE) {
        DesE = desE;
    }

    public String getImgE() {
        return ImgE;
    }

    public void setImgE(String imgE) {
        ImgE = imgE;
    }

    public void add(Equipe equipe) {
    }
}
