package com.btsinfo.myappi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Equipe>ListeEquipe;
    private DataBaseManager dbm;

    ListView lvListe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListeEquipe = new DataBaseManager(this).lectureEquipe();

        lvListe = (ListView)findViewById(R.id.listView1);

        lvListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                startViewActivity(i);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        ListeAdapter listeAdapter = new ListeAdapter(this, ListeEquipe);
        lvListe.setAdapter(listeAdapter);
    }

    private void startViewActivity(int i) {
        Equipe uneEquipe = ListeEquipe.get(i);

        Intent intent = new Intent(this, MembreActivity .class);

        intent.putExtra("idE",uneEquipe.getIdE());
        intent.putExtra("nomE",uneEquipe.getNomE());

        startActivity(intent);

    }
}