package com.btsinfo.myappi;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ligne2 extends AppCompatActivity {

    TextView tvpTitre1;
    TextView tvpTitre2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ligne2);


        tvpTitre1 = (TextView)findViewById(R.id.pTitre1);
        tvpTitre2 = (TextView)findViewById(R.id.pTitre2);

        Intent intent2 = getIntent();
        String nom = intent2.getStringExtra("nomP");
        String desc = intent2.getStringExtra("descP");
        tvpTitre1.setText(nom);
        tvpTitre2.setText(desc);
    }

}

