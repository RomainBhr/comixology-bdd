package com.btsinfo.myappi;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DataBaseManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="comics.db";
    private static final int DATABASE_VERSION = 9;

    public DataBaseManager (Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String strSqlE = "create table Equipe(idE int primary key,nomE String,desE String,imgE String)";
        db.execSQL(strSqlE);
        String strSqlM = "create table Membre(idE int,idP int,nomP String,desP string,desUn String,imgP String, primary key(idE,idP),FOREIGN KEY (idE) REFERENCES Equipe(idE))";
        db.execSQL(strSqlM);

        //insertion des valeurs
        String e1 = "insert into Equipe values (1,'Avengers','Super héros Marvel', 'avengers/Avengers.jpg')";
        String e2 = "insert into Equipe values (2,'Jla','Super héros DC', 'jla/jla.png')";
        String e3 = "insert into Equipe values (3,'X-Men','Super héros Mutants','xmen/xmen.png')";

        String m1 = "insert into Membre values (1,1,'Capitaine america','Super héros Marvel','Captain America est l alter ego de Steve Rogers, un jeune homme frêle amélioré au sommet de la perfection humaine par un sérum expérimental pour aider les efforts du gouvernement des États-Unis pendant la Seconde Guerre mondiale. Vers la fin de la guerre, il a été pris au piège dans la glace et a survécu en animation suspendue jusqu à ce qu il soit ranimé à l époque moderne.', 'avengers/capamerica.png')";
        String m2 = "insert into Membre values (1,2,'Guepe','Super héros Marvel','Sous sa forme de Guêpe, elle conserve sa force et sa résistance normale et possède des ailes lui permettant de voler à 60 km/h , résultat d une manipulation de son ADN par Pym. Elle peut communiquer télépathiquement avec les insectes.', 'avengers/guepe.png')";
        String m3 = "insert into Membre values (1,3,'Iron man','Super héros Marvel','Iron Man est un super-héros fictif qui apparaît dans les bandes dessinées américaines publiées par Marvel Comics. ... Plus tard, Stark développe son costume, en y ajoutant des armes et d autres dispositifs technologiques qu il a conçus par l intermédiaire de sa société, Stark Industries. Il utilise le costume et ses versions successives pour protéger le monde sous le nom d Iron Man.', 'avengers/ironman.png')";
        String m4 = "insert into Membre values (1,4,'Sorciere rouge','Super héros Marvel','La Sorcière rouge est à l origine une mutante qui possède le pouvoir de manipuler psioniquement les probabilités grâce à sa « magie hex ». Son pouvoir se manifeste typiquement sous la forme physique de « sphères hex » ou de « décharges hex ».', 'avengers/sorciererouge.png')";
        String m5 = "insert into Membre values (1,5,'Thor','Super héros Marvel', 'Dans l univers Marvel, Thor est un dieu de la mythologie nordique membre de la race des Asgardiens. Il possède comme tous les autres Asgardiens des caractéristiques physiques propres à sa race. Comme les autres Asgardiens, Thor n est pas un dieu immortel.','avengers/thor.png')";
        String m6 = "insert into Membre values (1,6,'Vif-Argent','Super héros Marvel','Vif-Argent est le fils de Magnéto, ancien leader de la Confrérie des mauvais mutants et grand ennemi des X-Men. ... Il est à l origine d un événement majeur de l univers Marvel, celui où 90 % de la population mutante perd ses pouvoirs. Il les a aussi perdus.', 'avengers/vifargent.png')";
        String m7 = "insert into Membre values (1,7,'Vision','Super héros Marvel','La Vision possède une force et une endurance surhumaine. En tant que robot intelligent, il peut se connecter à tout ordinateur. Son intelligence artificielle lui permet d enregistrer toute information.', 'avengers/vision.png')";
        String m8 = "insert into Membre values (2,8,'Aquaman','Super héros jla','Arthur Curry est Aquaman, un aventurier mi-humain mi-atlantéen. Cela lui confère une variété de pouvoirs, notamment une force surhumaine, un contrôle télépathique sur la vie marine et la capacité de survivre à la fois dans l eau et sur terre. Il appartient à la famille royale de l Atlantide, et agit normalement en tant que leur roi.','jla/aquaman.png')";
        String m9 = "insert into Membre values (2,9,'Batman','Super héros jla', 'Batman, est un super-héros de fiction appartenant à l univers de DC Comics. ... Batman n est en effet qu un simple humain qui a décidé de lutter contre le crime après avoir vu ses parents se faire abattre par un voleur dans une ruelle de Gotham City, la ville où se déroulent la plupart de ses aventures.','jla/batman.png')";
        String m10 = "insert into Membre values (2,10,'Cyborg','Super héros jla','Alimenté par un micro-ondes sous sa peau, il peut grimper aux murs et émettre une lumière aveuglante. Étant donné sa taille, on peut présumer que Cyborg a une force surhumaine.', 'jla/cyborg.png')";
        String m11 = "insert into Membre values (2,11,'Flash','Super héros jla','Barry Allen est The Flash, l homme le plus rapide du monde. En utilisant ses pouvoirs de super-vitesse, il se sert de la Speed Force et devient un combattant du crime costumé. Sa position est un héritage dans la famille Flash, successeur du premier Jay Garrick et prédécesseur de Wally West. Il est un membre fondateur de la Justice League of America.', 'jla/flash.png')";
        String m12 = "insert into Membre values (2,12,'Green Lantern','Super héros jla','Hal Jordan est membre, et parfois le chef, de la force de police intergalactique appelée le Green Lantern Corps. Il combat le mal à travers l Univers avec un anneau qui lui confère une variété de super-pouvoirs.', 'jla/greenlantern.png')";
        String m13 = "insert into Membre values (2,13,'Superman','Super héros jla','Superman réside et opère dans la ville américaine fictive de Metropolis. Sous le nom de Clark Kent, il est journaliste pour le Daily Planet, un journal de Metropolis. L intérêt de Superman pour l amour est généralement Lois Lane, et son ennemi juré est le super-vilain Lex Luther.', 'jla/superman.png')";
        String m14 = "insert into Membre values (2,14,'Wonder Woman','Super héros jla','Armée de ses bracelets à l épreuve des balles, de son lasso magique, et forte de son entraînement d amazone, Wonder Woman est l archétype de la femme parfaite dans l esprit de Marston. Elle est belle, intelligente, forte, mais a néanmoins un côté doux.', 'jla/wonderwoman.png')";
        String m15 = "insert into Membre values (3,15,'Angel','Super héros xmen','Angel est un mutant dont le principal pouvoir est sa capacité de voler dans les airs grâce à ses ailes.', 'xmen/angel.png')";
        String m16 = "insert into Membre values (3,16,'Cyclope','Super héros xmen',' Cyclope (« Cyclops » en version originale) est un super-héros évoluant dans l univers Marvel de la maison d édition Marvel Comics. ... Cyclope est un mutant qui a longtemps fait partie de l équipe de super-héros les X-Men. Il est aussi le frère d Alexander Summers, alias Havok.', 'xmen/cyclope.png')";
        String m17 = "insert into Membre values (3,17,'Fauve','Super héros xmen','Dans Ultimate Marvel, le Fauve a des mains et des pieds surdimensionnés. Il possède une force et une agilité surhumaines. Il est extrêmement intelligent.', 'xmen/fauve.png')";
        String m18 = "insert into Membre values (3,18,'Iceberg','Super héros xmen','L homme connu uniquement sous le nom d Iceberg est un seigneur du crime basé à New York, connu pour avoir travaillé temporairement pour Kingpin.', 'xmen/iceberg.png')";
        String m19 = "insert into Membre values (3,19,'Phenix','Super héros xmen', 'La Force du Phénix est l une des plus anciennes entités cosmiques connues, représentant la vie qui n est pas encore née, ainsi que les forces de création et de destruction. La Force du Phénix est une manifestation immortelle, indestructible et mutable de la force première universelle de la vie.','xmen/phenix.png')";

        db.execSQL(e1);
        db.execSQL(e2);
        db.execSQL(e3);

        db.execSQL(m1);
        db.execSQL(m2);
        db.execSQL(m3);
        db.execSQL(m4);
        db.execSQL(m5);
        db.execSQL(m6);
        db.execSQL(m7);
        db.execSQL(m8);
        db.execSQL(m9);
        db.execSQL(m10);
        db.execSQL(m11);
        db.execSQL(m12);
        db.execSQL(m13);
        db.execSQL(m14);
        db.execSQL(m15);
        db.execSQL(m16);
        db.execSQL(m17);
        db.execSQL(m18);
        db.execSQL(m19);

        Log.i("DATABASE","onCreate invoqué");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String strSqlE = "drop table Equipe";
        db.execSQL(strSqlE);
        String strSqlM = "Drop table Membre";
        db.execSQL(strSqlM);
        this.onCreate(db);
    }

    public ArrayList<Equipe> lectureEquipe(){
        ArrayList<Equipe> equipes = new ArrayList<>();
        String sqlE = "select idE, nomE, desE, imgE from Equipe";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlE, null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast()){
            Equipe equipe = new Equipe(curseur.getInt(0),curseur.getString(1),curseur.getString(2),curseur.getString(3));
            equipes.add(equipe);
            curseur.moveToNext();
        }
        curseur.close();
        return equipes;
    }
    public ArrayList<Personnage> lectureMembre(int equipeId){
        ArrayList<Personnage> personnages = new ArrayList<>();
        String sqlP = "select * from Membre where  idE = 1 ";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlP, null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast()){
            Personnage personnage = new Personnage(curseur.getInt(0),curseur.getInt(1),curseur.getString(2),curseur.getString(3),curseur.getString(4),curseur.getString(5));
            personnages.add(personnage);
            curseur.moveToNext();
        }
        curseur.close();
        return personnages;
    }
    public ArrayList<Personnage> lectureMembre2(int equipeId){
        ArrayList<Personnage> personnages = new ArrayList<>();
        String sqlP = "select * from Membre where  idE = 2 ";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlP, null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast()){
            Personnage personnage = new Personnage(curseur.getInt(0),curseur.getInt(1),curseur.getString(2),curseur.getString(3),curseur.getString(4),curseur.getString(5));
            personnages.add(personnage);
            curseur.moveToNext();
        }
        curseur.close();
        return personnages;
    }
    public ArrayList<Personnage> lectureMembre3(int equipeId){
        ArrayList<Personnage> personnages = new ArrayList<>();
        String sqlP = "select * from Membre where  idE = 3 ";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlP, null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast()){
            Personnage personnage = new Personnage(curseur.getInt(0),curseur.getInt(1),curseur.getString(2),curseur.getString(3),curseur.getString(4),curseur.getString(5));
            personnages.add(personnage);
            curseur.moveToNext();
        }
        curseur.close();
        return personnages;
    }


}
